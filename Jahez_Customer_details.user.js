// ==UserScript==
// @name         Jahez Script
// @namespace    http://tampermonkey.net/
// @version      0.5
// @description  try to take over the world!
// @author       You
// @match        https://portal.jahez.net/restaurant/restaurant_orders.htm?*
// @grant        none
// ==/UserScript==

(function() {
    var XHR = XMLHttpRequest.prototype;
    // Remember references to original methods
    var open = XHR.open;
    var send = XHR.send;
    // Overwrite native methods
    // Collect data:
    XHR.open = function(method, url) {
        this._method = method;
        this._url = url;
        return open.apply(this, arguments);
    };
    // Implement “ajaxSuccess” functionality
    XHR.send = function(postData) {

        this.addEventListener("load", function() {
            /* Method        */ this._method;
            /* URL           */ this._url;
            console.log("WORKS");
            if( typeof JSON.parse(this.responseText).Orders.customerPhoneNumber !== undefined) {
                console.log("WORKS2");
                $('#customerData').remove();
                $('#paymentMethod').parent().append("<p id='customerData'>Customer phone number: " + JSON.parse(this.responseText).Orders.customerPhoneNumber + "</br>Customer Name: " + JSON.parse(this.responseText).Orders.customerName + "</br>Driver phone: " + JSON.parse(this.responseText).Orders.driverPhone + "</br>Driver Name: " + JSON.parse(this.responseText).Orders.driverName +"</p>");
            }


            ///* Response body */ console.log(this.responseText);
            //          /* Request body  */ console.log(postData);
        });
        return send.apply(this, arguments);

    };
})();